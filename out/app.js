"use strict";
const express = require('express');
const request = require('request');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const GtfsRealtimeBindings = require('gtfs-realtime-bindings');
const inspect = require('eyes').inspector({ maxLength: 9999999 });
const staticDir = __dirname + '/../public';
app.use(express.static(staticDir));
io.on('connection', function (socket) {
    console.log('a user connected');
});
function fetch(type, cb) {
    const requestSettings = {
        method: 'GET',
        url: 'https://api.transport.nsw.gov.au/v1/gtfs/vehiclepos/' + type,
        encoding: null,
        headers: {
            'Authorization': 'apikey l7xxc0951591f8db475a9ab62d15111512f0'
        }
    };
    request(requestSettings, function (error, response, body) {
        console.log(error);
        const dupes = [];
        if (!error && response.statusCode === 200) {
            const feed = GtfsRealtimeBindings.FeedMessage.decode(body);
            feed.entity.forEach(function (entity) {
                if (dupes.indexOf(entity.vehicle.vehicle.id) === -1) {
                    dupes.push(entity.vehicle.vehicle.id);
                    inspect(entity);
                    io.emit(type, entity);
                }
            });
        }
        cb();
    });
}
function buses() {
    fetch('buses', function () {
        setTimeout(buses, 5000);
    });
}
buses();
function sydneytrains() {
    fetch('sydneytrains', function () {
        setTimeout(sydneytrains, 5000);
    });
}
sydneytrains();
function ferries() {
    fetch('ferries', function () {
        setTimeout(ferries, 5000);
    });
}
ferries();
server.listen(3000, function () {
    console.log("Demo Express server listening on port %d in %s mode", 3000, app.settings.env);
});
//# sourceMappingURL=app.js.map