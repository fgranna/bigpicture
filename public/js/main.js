// London
var coords = [-33.865, 151.209444];

var world = VIZI.world('world', {
  skybox: true,
  postProcessing: false
}).setView(coords);

// Set position of sun in sky
world._environment._skybox.setInclination(0.1);

//world._environment._skybox.setInclination(0.34);
// Set position of sun in sky
var inc = 0;

function incIt() {
  inc +=0.0005;
  world._environment._skybox.setInclination(inc);
  //setTimeout(incIt, 200);
}

world.on('postUpdate', () => {
  //incIt();
});



// Add controls
VIZI.Controls.orbit().addTo(world);

// CartoDB basemap
VIZI.imageTileLayer('http://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
}).addTo(world);
/*
// Buildings and roads from Mapzen (polygons and linestrings)
var topoJSONTileLayer = VIZI.topoJSONTileLayer('http://tile.mapzen.com/mapzen/vector/v1/all/{z}/{x}/{y}.topojson?api_key=vector-tiles-NT5Emiw', {
  interactive: true,
  output: true,
  //maxLOD: 18,
  distance: 50000,
  onEachFeature: function(feature, layer) {
    layer.on('click', function(layer, point2d, point3d, intersects) {
      console.log(layer);
      console.log(feature.properties);
      //var id = layer.feature.properties.LAD11CD;
      //var value = layer.feature.properties.POPDEN;

      //console.log(id + ': ' + value, layer, point2d, point3d, intersects);
    });
  },

  style: function(feature) {
    var height;

    if (feature.properties.height) {
      height = feature.properties.height;
    } else {
      height = 10 + Math.random() * 10;
    }
    let lineColor = '#f7c616';
    let lineWidth = 2;
    if (feature.properties.route) {
      lineColor = '#A1D490';
    }
    if (feature.properties.kind === 'rail') {
        lineColor = '#FF0000';
        lineWidth = 10;
    }


    return {
      height: height,
      lineColor: lineColor,
      lineWidth: lineWidth,
      //lineTransparent: true,
      //lineOpacity: 0.2,
      lineBlending: THREE.AdditiveBlending,
      lineRenderOrder: 1
    };
  },
  filter: function(feature) {
    //console.log(feature.geometry.type);
    // Don't show points
    return (
      feature.geometry.type !== 'Point'
    );
  },
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://whosonfirst.mapzen.com#License">Who\'s On First</a>.'
});//.addTo(world);
*/
var socket = io();

// Buildings and roads from Mapzen (polygons and linestrings)
var topoJSONTileLayer = VIZI.topoJSONTileLayer('http://tile.mapzen.com/mapzen/vector/v1/buildings,transit/{z}/{x}/{y}.topojson?api_key=vector-tiles-NT5Emiw', {
  interactive: false,
  style: function(feature) {
    var height;

    if (feature.properties.height) {
      height = feature.properties.height;
    } else {
      height = 10 + Math.random() * 10;
    }

    return {
      height: height,
      lineColor: '#f7c616',
      lineWidth: 2,
      lineTransparent: false,
      lineOpacity: 0.8,
      lineBlending: THREE.AdditiveBlending,
      lineRenderOrder: 2
    };
  },
  filter: function(feature) {
    // Don't show points
    return feature.geometry.type !== 'Point';
  },
  attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://whosonfirst.mapzen.com#License">Who\'s On First</a>.'
}).addTo(world);

let vehicles = {};

socket.on('sydneytrains', function(msg){
  const id = msg.vehicle.vehicle.id;
  const pos = msg.vehicle.position;
  if (!vehicles[id]) {
    console.log('Adding',msg.vehicle.vehicle.label);
    vehicles[id] = new VIZI.PointLayer([pos.longitude,pos.latitude], {
      interactive: true,
      output: true,
      geometry: new THREE.BoxGeometry( 60, 60, 60 ),
      material: new THREE.MeshLambertMaterial( {
        transparent: false,
        opacity: 0.8,
        color: 0xBD631A
      }),
      style: {
        //height: 10,
        pointHeight: 30,
        pointColor: '#FF0000'
      }
    });
    vehicles[id].vehicleData = msg;
    vehicles[id].on('click', function(layer, point2d, point3d, intersects) {
      console.log(layer.vehicleData);
    });
    vehicles[id].addTo(world);
  } else {
    const newll = new VIZI.LatLon(pos.latitude, pos.longitude);
    const oldpoint = vehicles[id]._projectedCoordinates[0];
    const newpoint = world.latLonToPoint(newll);

    const tween = new TWEEN.Tween(oldpoint).easing(TWEEN.Easing.Quartic.InOut).to(newpoint, 2300);
    tween.onUpdate(function() {
      vehicles[id]._mesh.position.x = oldpoint.x + vehicles[id]._offset.x;
      vehicles[id]._mesh.position.z = oldpoint.y + vehicles[id]._offset.y;
      vehicles[id]._pickingMesh.position.x = oldpoint.x + vehicles[id]._offset.x;
      vehicles[id]._pickingMesh.position.z = oldpoint.y + vehicles[id]._offset.y;
    });
    tween.start();
  }
  //console.log(msg);
});

socket.on('ferries', function(msg){
  const id = msg.vehicle.vehicle.id;
  const pos = msg.vehicle.position;
  if (!vehicles[id]) {
    console.log('Adding',msg.vehicle.vehicle.label);
    vehicles[id] = new VIZI.PointLayer([pos.longitude,pos.latitude], {
      interactive: true,
      output: true,
      geometry: new THREE.BoxGeometry( 60, 60, 60 ),
      material: new THREE.MeshLambertMaterial( {
        transparent: false,
        opacity: 0.8,
        color: 0x1E9618
      }),
      style: {
        //height: 10,
        pointHeight: 30,
        pointColor: '#FF0000'
      }
    });
    vehicles[id].vehicleData = msg;
    vehicles[id].on('click', function(layer, point2d, point3d, intersects) {
      console.log(layer.vehicleData);
    });
    vehicles[id].addTo(world);
  } else {
    const newll = new VIZI.LatLon(pos.latitude, pos.longitude);
    const oldpoint = vehicles[id]._projectedCoordinates[0];
    const newpoint = world.latLonToPoint(newll);

    const tween = new TWEEN.Tween(oldpoint).easing(TWEEN.Easing.Quartic.InOut).to(newpoint, 2300);
    tween.onUpdate(function() {
      vehicles[id]._mesh.position.x = oldpoint.x + vehicles[id]._offset.x;
      vehicles[id]._mesh.position.z = oldpoint.y + vehicles[id]._offset.y;
      vehicles[id]._pickingMesh.position.x = oldpoint.x + vehicles[id]._offset.x;
      vehicles[id]._pickingMesh.position.z = oldpoint.y + vehicles[id]._offset.y;
    });
    tween.start();
  }
  //console.log(msg);
});

socket.on('buses', function(msg){
  const id = msg.vehicle.vehicle.id;
  const pos = msg.vehicle.position;
  if (!vehicles[id]) {
    console.log('Adding',msg.vehicle.vehicle.label);
    vehicles[id] = new VIZI.PointLayer([pos.longitude,pos.latitude], {
      interactive: true,
      output: true,
      geometry: new THREE.BoxGeometry( 60, 60, 60 ),
      material: new THREE.MeshLambertMaterial( {
        transparent: false,
        opacity: 0.8,
        color: 0x191D9C
      }),
      style: {
        //height: 10,
        pointHeight: 30,
        pointColor: '#FF0000'
      }
    });
    vehicles[id].vehicleData = msg;
    vehicles[id].on('click', function(layer, point2d, point3d, intersects) {
      console.log(layer.vehicleData);
    });
    vehicles[id].addTo(world);
  } else {
    const newll = new VIZI.LatLon(pos.latitude, pos.longitude);
    const oldpoint = vehicles[id]._projectedCoordinates[0];
    const newpoint = world.latLonToPoint(newll);

    const tween = new TWEEN.Tween(oldpoint).easing(TWEEN.Easing.Quartic.InOut).to(newpoint, 2300);
    tween.onUpdate(function() {
      vehicles[id]._mesh.position.x = oldpoint.x + vehicles[id]._offset.x;
      vehicles[id]._mesh.position.z = oldpoint.y + vehicles[id]._offset.y;
      vehicles[id]._pickingMesh.position.x = oldpoint.x + vehicles[id]._offset.x;
      vehicles[id]._pickingMesh.position.z = oldpoint.y + vehicles[id]._offset.y;
    });
    tween.start();
  }
  //console.log(msg);
});

const pl = new VIZI.PointLayer([151.209444,-33.865], {
  interactive: true,
  output: true,
  height:10,
  geometry: new THREE.SphereGeometry( 50, 32, 32 ),
  material: new THREE.MeshLambertMaterial( {
    transparent: false,
    opacity: 0.6,
    color: 0x0000ff
  }),
  style: {
    //height: 10,
    pointHeight: 100,
    pointColor: '#FF0000'
  },
  onEachFeature: function(feature, layer) {
    console.log(layer);
    layer.on('click', function(layer, point2d, point3d, intersects) {
      console.log(layer);
    });
  }
});
pl.on('click', () => {
  console.log('click');
});
console.log(pl);
/*
pl.addTo(world);

const newll = new VIZI.LatLon(-33.93994207,151.17526531);
//console.log('con', pl._projectedCoordinates[0]);
const oldpoint = pl._projectedCoordinates[0];
const newpoint = _point = world.latLonToPoint(newll);

const tween = new TWEEN.Tween(oldpoint).easing(TWEEN.Easing.Bounce.Out).to(newpoint, 2300).repeat(Infinity).delay(500).yoyo(true);
tween.onUpdate(function() {
  //pl._mesh.position.set(new THREE.Vector3(oldpoint.x, 0, oldpoint.y));
  //console.log('update', oldpoint);
  pl._mesh.position.x = oldpoint.x;
  pl._mesh.position.z = oldpoint.y;
});
tween.start();
*/
world.on('preUpdate', (delta) => {
  TWEEN.update();
});
