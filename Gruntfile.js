'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    clean: {
      vendor: ['vendor/vizicities/']
    },
    gitclone: {
      vizicities: {
        options: {
          repository: 'https://github.com/UDST/vizicities.git',
          branch: 'dev',
          directory: 'vendor/vizicities',
          verbose: true
        }
      }
    },
    concat: {
      vizicities: {
        src: [
          'vendor/vizicities/dist/vizicities.js',
          //'src/vizicities/**/*.js'
        ],
        dest: 'public/js/dist/vizicities.js'
      }
    },
    copy: {
      vizicities_css: {
        flatten: true,
        expand: true,
        src: 'vendor/vizicities/dist/*.css',
        dest: 'public/css/',
        filter: 'isFile'
      },
      dist: {
        flatten: true,
        expand: true,
        src: 'vendor/*.js',
        dest: 'public/js/dist/',
        filter: 'isFile'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-git');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.registerTask('fetchdeps', [
    'clean:vendor',
    'gitclone:vizicities'
  ]);

  grunt.registerTask('build', [
    'concat:vizicities',
    'copy:vizicities_css',
    'copy:dist'
  ]);
};
